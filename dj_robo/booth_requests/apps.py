from django.apps import AppConfig


class BoothRequestsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'booth_requests'
